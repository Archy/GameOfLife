﻿using System;
using System.IO;

namespace GameOfLife.Model
{
    public enum CellState
    {
        EMPTY,
        ALIVE,
        NEW,
        DYING
    }

    class GameOfLifeModel
    {
        public CellState[,] Cells { get; set; }

        public GameOfLifeModel(int width, int height)
        {
            Cells = new CellState[height, width];
        }

        public void changeCell(int x, int y)
        {
            if (Cells[y,x] == CellState.ALIVE || Cells[y,x] == CellState.NEW)
                Cells[y,x] = CellState.EMPTY;
            else
                Cells[y,x] = CellState.ALIVE;
        }

        public void resize(int width, int height)
        {
            CellState[,] temp = new CellState[height, width];

            for (int i = 0; i<temp.GetLength(0) && i<Cells.GetLength(0); ++i)
            {
                for (int j = 0; j<temp.GetLength(1) && j<Cells.GetLength(1); ++j)
                {
                    temp[i, j] = Cells[i,j];
                }
            }
            Cells = temp;
        }

        public void next()
        {
            CellState[,] temp = new CellState[Cells.GetLength(0), Cells.GetLength(1)];
            for (int i = 0; i < Cells.GetLength(0); ++i)
            {
                for (int j = 0; j < Cells.GetLength(1); ++j)
                {
                    int neigbours = countNeighbours(j, i);
                    if(neigbours == 2)
                    {
                        if (Cells[i, j] == CellState.ALIVE || Cells[i, j] == CellState.NEW)
                        {
                            temp[i, j] = CellState.ALIVE;
                        }
                        else
                        {
                            temp[i, j] = CellState.EMPTY;
                        }
                    }
                    else if (neigbours == 3)
                    {
                        if (Cells[i, j] == CellState.ALIVE || Cells[i, j] == CellState.NEW)
                        {
                            temp[i, j] = CellState.ALIVE;
                        }
                        else
                        {
                            temp[i, j] = CellState.NEW;
                        }
                    }
                    else
                    {
                        if(Cells[i, j] == CellState.ALIVE || Cells[i, j] == CellState.NEW)
                        {
                            temp[i, j] = CellState.DYING;
                        }
                        else
                        {
                            temp[i, j] = CellState.EMPTY;
                        }
                    }
                }
            }
            Cells = temp;
        }

        private int countNeighbours(int x, int y)
        {
            int neigbours = 0;
            for(int i=y-1; i<=y+1; ++i)
            {
                for(int j=x-1; j<=x+1; ++j)
                {
                    if(0<=i && i<Cells.GetLength(0) 
                        && 0<=j && j<Cells.GetLength(1) 
                        && !(i==y && j==x) 
                        && (Cells[i,j] == CellState.ALIVE || Cells[i,j]==CellState.NEW))
                    {
                        ++neigbours;
                    }
                }
            }
            return neigbours;
        }

        public void save(string fileName)
        {
            using (StreamWriter outputFile = new StreamWriter(fileName))
            {
                outputFile.WriteLine(Cells.GetLength(0) + " " + Cells.GetLength(1));
                foreach(CellState cell in Cells)
                {
                    outputFile.Write(cell + " ");
                }
            }
        }

        public void open(string fileName)
        {
            using (StreamReader inputFile = new StreamReader(fileName))
            {
                var size = inputFile.ReadLine().Split(' ');
                int height = int.Parse(size[0]), width = int.Parse(size[1]);
                string[] words = inputFile.ReadToEnd().Split(' ');

                var temp = new CellState[height, width];
                for (int i = 0; i < height; ++i)
                {
                    for (int j = 0; j < width; ++j)
                    {
                        CellState cell;
                        if (!Enum.TryParse(words[i*width + j], out cell))
                            throw new InvalidDataException("Unknown cell type: " + words[i * width + j]);
                        temp[i, j] = cell;
                    }
                }
                Cells = temp;
            }
        }
    }
}
