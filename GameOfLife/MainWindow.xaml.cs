﻿using GameOfLife.ViewModel;
using Microsoft.Win32;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace GameOfLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GameOfLifeVM game = new GameOfLifeVM();

        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.WidthAndHeight;
            this.DataContext = game;

            var cellsGrid = (Grid)this.FindName("cellsGrid");
            cellsGrid.Resources.Add("theGame", game);
        }

        private void mouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Point clickPoint = e.GetPosition((Grid)this.FindName("cellsGrid"));
            game.click((int)clickPoint.X / 25, (int)clickPoint.Y / 25);
        }

        private void nextClick(object sender, RoutedEventArgs e)
        {
            game.next();
        }

        private void textBoxUpdate(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BindingExpression binding = BindingOperations.GetBindingExpression((TextBox)sender, TextBox.TextProperty);
                binding?.UpdateSource();
            }
        }

        private void saveFile(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = "game-of-life.txt";
            dialog.Filter = "Text File | *.txt";
            if (dialog.ShowDialog() == true)
                game.save(dialog.FileName);

        }

        private void openFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
                game.open(dialog.FileName);
        }
    }
}
