﻿using GameOfLife.Model;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace GameOfLife.ViewModel
{
    public class CellToColorConverter : IValueConverter
    {
        private static SolidColorBrush aliveColor = new SolidColorBrush(Color.FromRgb(0, 0, 153));
        private static SolidColorBrush newColor = new SolidColorBrush(Color.FromRgb(51, 102, 255));
        private static SolidColorBrush dyingColor = new SolidColorBrush(Color.FromRgb(179, 179, 179));


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((CellState)value)
            {
                case CellState.ALIVE:
                    return aliveColor;
                case CellState.NEW:
                    return newColor;
                case CellState.DYING:
                    return dyingColor;
                default:
                    return Brushes.White;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GameOfLifeVM : INotifyPropertyChanged
    {
        private int width = 20;
        private int height = 20;
        private GameOfLifeModel game = new GameOfLifeModel(20, 20);

        public int Step { get; set; } = 1;
        public ObservableCollection<ObservableCollection<CellState>> Cells { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public int Width
        {
            get { return width; }
            set { width = value; OnSizeChange(); }
        }
        public int Height
        {
            get { return height; }
            set { height = value; OnSizeChange(); }
        }


        public GameOfLifeVM()
        {
            Cells = new ObservableCollection<ObservableCollection<CellState>>();
            for(int i=0; i< Height; ++i)
            {
                Cells.Add(new ObservableCollection<CellState>());
                for(int j=0; j< Width; ++j)
                {
                    Cells[i].Add(CellState.EMPTY);
                }
            }
        }

        public void next()
        {
            for(int i=0; i<Step; ++i)
            {
                game.next();
            }
            updateViewModel();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Cells"));
        }

        public void click(int x, int y)
        {
            game.changeCell(x, y);
            updateViewModel();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Cells"));
        }

        public void save(string fileName)
        {
            game.save(fileName);
        }

        public void open(string fileName)
        {
            game.open(fileName);
            height = game.Cells.GetLength(0);
            width = game.Cells.GetLength(1);
            resize();
            updateViewModel();

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Height"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Width"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Cells"));
        }

        private void OnSizeChange()
        {
            assureSize();
            resize();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Cells"));
        }

        private void resize()
        {
            game.resize(Width, Height);
            var temp = new ObservableCollection<ObservableCollection<CellState>>();
            for (int i = 0; i < Height; ++i)
            {
                temp.Add(new ObservableCollection<CellState>());
                for (int j = 0; j < Width; ++j)
                {
                    temp[i].Add(CellState.EMPTY);
                }
            }
            Cells = temp;
            updateViewModel();
        }

        private void assureSize()
        {
            if(width < 5)
            {
                width = 5;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Width"));
            } else if (width > 35)
            {
                width = 35;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Width"));
            }

            if (height < 5)
            {
                height = 5;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Height"));
            }
            else if (height > 35)
            {
                height = 35;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Height"));
            }
        }

        private void updateViewModel()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    Cells[i][j] = game.Cells[i, j];
                }
            }
        }
    }
}
